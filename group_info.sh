#!/bin/bash
#set -x

#group id
GROUP_ID="CHANGEME"

#https://$GITLAB_URL/-/profile/personal_access_tokens
#GITLAB_TOKEN="changeme"
GITLAB_TOKEN=$(cat source_gitlab_api.token)

#gitlab base url
GITLAB_URL="https://example.gitlab.com/"

#base path to projects
BASE_FILE="/tmp/repos.list"
BASE_DIR="/tmp/"

API="${GITLAB_URL}api/v4/"
REPOS=()


# need to investigate windows bash arrays.
# dont work on windows@mingw64
#windows: curl -L -o /usr/bin/jq.exe https://github.com/stedolan/jq/releases/latest/download/jq-win64.exe
JQ_PATH=$(which jq)
if [ ! -f $JQ_PATH ]
then
    echo "install jq"
    exit 0
fi

CURL_PATH=$(which curl)
if [ ! -f $CURL_PATH ]
then
    echo "install curl"
    exit 0
fi


#simplify curl command
# GET
gcurl () {
  curl -s --request GET --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $@
}
# POST
pcurl () {
  curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $@
}
# PATCH
patchcurl () {
  curl -s --request PATCH --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $@
}
# DELETE
delcurl () {
  curl -s --request DELETE --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $@
}




PAGES=$(curl -s --head "${API}groups/${GROUP_ID}/projects?include_subgroups=true&private_token=$GITLAB_TOKEN" | grep -i x-total-pages | awk '{print $2}' | tr -d '\r\n')
PROJECTS=()

for page in $(seq 1 $PAGES); do
#    gcurl "${API}groups/${GROUP_ID}/projects?include_subgroups=true&page=$page" | jq -r '.[].id'
    PROJECTS+=' '
    PROJECTS+=$( gcurl "${API}groups/${GROUP_ID}/projects?include_subgroups=true&page=$page" | jq -r '.[].id' )
done

echo "" > $BASE_FILE
for PROJECT in ${PROJECTS[@]}
do
  RESPONCE=$(gcurl "${API}projects/$PROJECT")
  # echo $RESPONCE | jq .
  ID=$(echo $RESPONCE | jq -r '.id')
  NAME=$(echo $RESPONCE | jq -r '.name')
  GIT_PATH=$(echo $RESPONCE | jq -r '.path_with_namespace' | rev | cut -d/ -f2- | rev)
  GIT_NAME=$(echo $RESPONCE | jq -r '.path_with_namespace' | rev | cut -d/ -f1 | rev)
  GIT_URL=$(echo $RESPONCE | jq -r '.ssh_url_to_repo')
  WEB_URL=$(echo $RESPONCE | jq -r '.web_url')
  VISIBILITY=$(echo $RESPONCE | jq -r '.visibility')
  REPO=$(echo "{\"name\": \"$NAME\", \"web_url\": \"$WEB_URL\", \"git_path\": \"$GIT_PATH\", \"git_name\": \"$GIT_NAME\", \"visibility\": \"$VISIBILITY\" }")
  echo $REPO >> $BASE_FILE
  REPOS+=$REPO
#  echo $REPO >> /tmp/repos.list
#  echo "Will clone $NAME to $BASE_DIR/$GIT_PATH/$GIT_NAME from $GIT_URL"
#  mkdir -p $BASE_DIR/$GIT_PATH/
#  git clone $GIT_URL $BASE_DIR/$GIT_PATH/$GIT_NAME
done

#JSON_REPOS=$(jq --compact-output --null-input '$ARGS.positional' --args -- ${REPOS[@]})
#cat /tmp/repos.list | jq -cs
