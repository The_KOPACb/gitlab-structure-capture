#!/usr/bin/env python

import requests
from pprint import pprint
import json

# configuration
TARGET_GROUP_ID = "CHANGEME"
IMPORTED_BASE_GROUP = 'platform'

# gitlab base url
GITLAB_URL = "https://example.gitlab.com/"
# GITLAB_TOKEN=$(cat repository_rw_api_repo.token)
GITLAB_TOKEN_FILE = 'target_gitlab_api.token'

# import settings
PROGECTS = "./project_list.txt"

IMPORTED_BASE_GROUP_LEN = IMPORTED_BASE_GROUP.count('/') + 1
CREATE_ALL_INSIDE = False

if GITLAB_TOKEN_FILE:
    with open(GITLAB_TOKEN_FILE, 'r') as file:
        GITLAB_TOKEN = file.read().replace('\n', '')

def main():
    # init
    api = Gitlab(GITLAB_URL, GITLAB_TOKEN)
    target = api.get('groups/{}'.format(TARGET_GROUP_ID))
    # TARGET_GROUP_PATH = target['path']
    TARGET_GROUP_PATH = target['full_path']
    TARGET_GROUP_PATH_LEN = TARGET_GROUP_PATH.count('/')
    TARGET_GROUP_FULL_PATH = target['full_path']
    TARGET_GROUP_FULL_PATH_LEN = TARGET_GROUP_FULL_PATH.count('/')

    with open(PROGECTS) as f:
        projects = f.read().splitlines()
    target_projects = []
    for project in projects:
        target_projects.append(json.loads(project))

    raw_target_subgroups = []
    for project in target_projects:
        if project['git_path'] not in raw_target_subgroups:
            # raw_target_subgroups.append('{}/{}'.format(TARGET_GROUP_PATH, project['git_path'].split('/', IMPORTED_BASE_GROUP_LEN)[:IMPORTED_BASE_GROUP_LEN]))
            tsg = project['git_path'].split('/', IMPORTED_BASE_GROUP_LEN)[:IMPORTED_BASE_GROUP_LEN + 1]
            try:
                raw_target_subgroups.append('{}/{}'.format(TARGET_GROUP_PATH, tsg[1]))
            except IndexError:
                pass

    raw_subgroups_to_create = []
    for subgroup in raw_target_subgroups:
        # ss = subgroup.split('/', IMPORTED_BASE_GROUP_LEN)
        # subgroup = '/'.join(ss[IMPORTED_BASE_GROUP_LEN:])
        # subgroup = subgroup.split('/', IMPORTED_BASE_GROUP_LEN)[int(IMPORTED_BASE_GROUP_LEN + 1)]
        if subgroup not in raw_subgroups_to_create:
            raw_subgroups_to_create.append(subgroup)
        if '/' in subgroup:
            for i in range(1, subgroup.count('/') + 1 - TARGET_GROUP_FULL_PATH_LEN):
                if subgroup.rsplit('/', i)[0] not in raw_subgroups_to_create:
                    raw_subgroups_to_create.append(subgroup.rsplit('/', i)[0])

    raw_subgroups_to_create.sort(key=lambda x: x.count('/'))
    # print('raw_subgroups_to_create')
    # print(raw_subgroups_to_create)

    # body
    _, sub_headers = api.get('groups/{}/descendant_groups?include_subgroups=true'.format(TARGET_GROUP_ID),
                             get_headers=True)
    groups_total_pages = int(sub_headers['X-Total-Pages'])

    raw_ex_subgroups = []
    for i in range(1, groups_total_pages + 1):
        raw_ex_subgroups += api.get(
            'groups/{}/descendant_groups?include_subgroups=true&page={}'.format(TARGET_GROUP_ID, i))

    ex_subgroups = dict()
    ex_subgroups = {TARGET_GROUP_PATH: TARGET_GROUP_ID}
    for ex_subgroup in raw_ex_subgroups:
        # fp = ex_subgroup['full_path'].split('/', TARGET_GROUP_PATH_LEN+1)
        # full_path = '/'.join(fp[:TARGET_GROUP_FULL_PATH_LEN+1])
        # print(full_path)
        ex_subgroups[ex_subgroup['full_path']] = ex_subgroup['id']
    # print('ex sg')
    # print(ex_subgroups)

    print('for subgroup in raw_subgroups_to_create:')
    subgroups_to_create = []
    manual_subgroups_to_create = []
    for subgroup in raw_subgroups_to_create:
        print(subgroup)
        if CREATE_ALL_INSIDE:
            print('in development')
            exit(0)
        else:
            if '{}/'.format(TARGET_GROUP_PATH) in subgroup:
                if not '{}'.format(subgroup) in ex_subgroups:
                    # subgroup_to_create = subgroup.replace('{}/'.format(IMPORTED_BASE_GROUP), '{}/'.format(TARGET_GROUP_PATH))
                    subgroups_to_create.append(subgroup)
            else:
                if subgroup != IMPORTED_BASE_GROUP:
                    manual_subgroups_to_create.append(subgroup)
                else:
                    pass

    subgroups_to_create.sort(key=lambda x: x.count('/'))
    # print('subgroups to create')
    # pprint(subgroups_to_create)
    # t_ex_subgroups = dict()
    # for subgroup in ex_subgroups:
    #     ss = subgroup.split('/', TARGET_GROUP_PATH_LEN)
    #     ssubgroup = '/'.join(ss[:])
    #     print(subgroup, TARGET_GROUP_PATH_LEN, ss, ssubgroup)
    #     t_ex_subgroups[ssubgroup] = ex_subgroups[subgroup]

    for subgroup in subgroups_to_create:
        print(subgroup)
        if subgroup not in ex_subgroups:
            path = subgroup.rsplit('/', 1)[0]
            group = subgroup.rsplit('/', 1)[1]
            print('p, g', path, group)
            if path in ex_subgroups:
                parent_id = ex_subgroups[path]
            else:
                parent_id = ex_subgroups[TARGET_GROUP_PATH]
            payload = {"path": group, "name": group, "parent_id": parent_id}
            responce = api.post('groups', payload)
            ex_subgroups[responce['full_path']] = responce['id']
        else:
            manual_subgroups_to_create.append(subgroup)

    _, sub_headers = api.get('groups/{}/projects?include_subgroups=true'.format(TARGET_GROUP_ID), get_headers=True)
    groups_total_pages = int(sub_headers['X-Total-Pages'])
    ex_projects = []
    for i in range(1, groups_total_pages + 1):
        ex_projects += api.get('groups/{}/projects?include_subgroups=true&page={}'.format(TARGET_GROUP_ID, i))

    # for ex_project in ex_projects:
    #     print(ex_project)

    projects_to_export = []
    for project in target_projects:
        if project['git_path'].startswith('{}'.format(IMPORTED_BASE_GROUP)):
            target_group = project['git_path'].replace(IMPORTED_BASE_GROUP, TARGET_GROUP_FULL_PATH)
            payload = {
                "name": project['name'],
                "description": project['name'],
                "path": '{}'.format(project['git_name'].lower()),
                "namespace_id": '{}'.format(ex_subgroups[target_group]),
                "initialize_with_readme": "False"}
            responce = api.post('projects', payload)
            if 'message' in responce.keys() and responce['message']['path'] == ['has already been taken']:
                target_project = list(filter(
                    lambda ex_project: ex_project['path_with_namespace'] == '{}/{}'.format(target_group,
                                                                                           project['git_name']),
                    ex_projects))[0]
                project_to_export = dict()
                project_to_export['ssh_url_to_repo'] = target_project['ssh_url_to_repo']
                project_to_export['http_url_to_repo'] = target_project['http_url_to_repo']
                project_to_export['path_with_namespace'] = target_project['path_with_namespace']
                project_to_export['web_url'] = target_project['web_url']
                project_to_export['name'] = target_project['name']
                back_response = {'project': project, 'new': project_to_export}
                projects_to_export.append(back_response)
            else:
                project_to_export = dict()
                project_to_export['ssh_url_to_repo'] = responce['ssh_url_to_repo']
                project_to_export['http_url_to_repo'] = responce['http_url_to_repo']
                project_to_export['path_with_namespace'] = responce['path_with_namespace']
                project_to_export['web_url'] = responce['web_url']
                project_to_export['name'] = responce['name']
                back_response = {'project': project, 'new': project_to_export}
                projects_to_export.append(back_response)

    export = json.dumps(projects_to_export)
    pprint(export)
    with open('response.json', 'w', encoding='utf-8') as f:
        json.dump(projects_to_export, f, ensure_ascii=False, indent=4)


class Gitlab:
    def __init__(self, gitlab_url, gitlab_token):
        self.api = '{}/api/v4/'.format(gitlab_url)
        self.auth = {'PRIVATE-TOKEN': gitlab_token}

    def get(self, endpoint, headers={}, get_headers=False):
        url = '{}{}'.format(self.api, endpoint)
        request_headers = self.auth
        request_headers.update(headers)
        response = requests.get(url, headers=request_headers)
        h = response.headers
        j = response.json()
        if get_headers:
            return j, h
        else:
            return j

    def post(self, endpoint, payload, headers={}):
        url = '{}{}'.format(self.api, endpoint)
        request_headers = self.auth
        request_headers.update(headers)
        response = requests.post(url, headers=request_headers, data=payload)
        response = response.json()
        return response


def split(strng, pos):
    sep = '/'
    strng = strng.split(sep)
    return sep.join(strng[:pos]), sep.join(strng[pos:])


if __name__ == '__main__':
    main()
