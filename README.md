# Gitlab structure capture
This scripts allows to transfer group structure between gitlab instances

# group_info.sh

Configure 
```bash
GROUP_ID="CHANGEME"

#https://$GITLAB_URL/-/profile/personal_access_tokens
GITLAB_TOKEN="changeme"
#GITLAB_TOKEN=$(cat gitlab_api.token)

#gitlab base url
GITLAB_URL="https://example.gitlab.com/"

BASE_FILE="/tmp/repos.list"
```
 in group_info.sh and it will grab projects to `$BASE_FILE`


 # gitlab-populate.py

It needs only `requests` lib, so
    `pip install requests`
    or
    `pip install -r requirements.txt`

 Configure 
 ```python
# configuration
TARGET_GROUP_ID = "CHANGEME"
IMPORTED_BASE_GROUP = 'platform'

# gitlab base url
GITLAB_URL = "https://example.gitlab.com/"
# GITLAB_TOKEN=$(cat repository_rw_api_repo.token)
GITLAB_TOKEN_FILE = 'target_gitlab_api.token'

# import settings
PROGECTS = "./project_list.txt"
 ```

* `TARGET_GROUP_ID` - id of target group ( where structure will be created )
* `IMPORTED_BASE_GROUP` - name of base group to import
* `GITLAB_URL` - url to gitlab instance, trailing slash is important
* use on of these: 
  * `GITLAB_TOKEN` - gitlab api rw token
  * `GITLAB_TOKEN_FILE` - file containing gitlab token
* `PROGECTS` - file with list of projects

and it will recreate saved group structure in `TARGET_GROUP_ID`

